
# PHP Add Watermark On Image

```php

<?php
$path = "";
$watermark_path = "";
$imagename = "imagename_here.png";
$newimagename = "new_image_name_with_watermark_here.".pathinfo($imagename, PATHINFO_EXTENSION);
$watermark = "watermark_here.png"; //Watermark Only PNG
function convert($image, $wtrmrk_file, $new_image,$watermark_position) {
    $watermark = imagecreatefrompng($wtrmrk_file);
    imagealphablending($watermark, false);
    imagesavealpha($watermark, true);
    
    if(pathinfo($image, PATHINFO_EXTENSION) == "png"){
        
        $img = imagecreatefrompng($image);
        
    }else{
        
        $img = imagecreatefromjpeg($image);
        
    }
    
    $img_w = imagesx($img);
    $img_h = imagesy($img);
    $wtrmrk_w = imagesx($watermark);
    $wtrmrk_h = imagesy($watermark);
    $dst_x = ($img_w / 2) - ($wtrmrk_w / 2);
    $dst_y = ($img_h / 2) - ($wtrmrk_h / 2);
    imagecopy($img, $watermark, $dst_x, $dst_y, 0, 0, $wtrmrk_w, $wtrmrk_h);
    if(pathinfo($image, PATHINFO_EXTENSION) == "png"){
        
        imagesavealpha($img, true);
        imagepng($img, $new_image, 9);
        
    }else{
        
        imagejpeg($img, $new_image, 100);         
    }
    
    imagedestroy($img);
    imagedestroy($watermark);
}
convert($path.$imagename, $watermark_path.$watermark, $path.$newimagename,$watermark);
?>
```